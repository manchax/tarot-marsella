# Introduction 
Marseilles Tarot reader that uses Text to Speech (TTS) synthesis.

Main functionality is in [Tarot Reader Service](./Tarot.Marsella/Services/TarotReader.cs)

# Getting Started
TTS requires Windows.

Application's UI is built on top of .NET MAUI using .NET 7 targeting Windows 10 or 11. It also goes well with Android.

# Build and Test
1. Build with Visual Studio.
2. Verify that your Windows Speech system settings have at least one French (France) and a Spanish (Spain) voices.
3. Run the solution.
