﻿
namespace Tarot.Marsella;

public partial class App : Application
{
    public App()
    {
        InitializeComponent();
    }

    protected override Window CreateWindow(IActivationState activationState) => new(page: new AppShell())
    {
        Title = "Tarot de Marsella"
    };
}