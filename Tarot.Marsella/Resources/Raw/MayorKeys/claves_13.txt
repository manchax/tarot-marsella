﻿Cambio,Aceptar el,No aceptar el
Mutación,Aceptar la,No aceptar la
Revolución,para bien,para mal
Ira,Constructora,Destructora
Transformación,para bien,para mal
Limpieza,Buena,Mala
Cosecha,Buena,Mala
Esqueleto,fuerte,débil
Cortar,lo malo,lo bueno
Avanzar,sin mirar atrás,mirando al pasado
Eliminar,lo malo,lo bueno
Destruir,para construir,sin construir
Rapidez,Ir con,No ir con