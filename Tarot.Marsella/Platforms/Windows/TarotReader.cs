﻿namespace Tarot.Marsella.Services;

partial class TarotReader
{
    private partial Locale FindLocale(string langCode)
    {
        var r = from l in _locales
                where l.Language.StartsWith(langCode)
                select l;

        if (!r.Any())
        {
            throw new ApplicationException(
                "Requested TTS locale not found for " + langCode);
        }

        return r.First();
    }
}