﻿namespace Tarot.Marsella.Services;

partial class TarotReader
{
    private partial Locale FindLocale(string langCode)
    {
        if (!_locales.Any())
        {
            Task.Run(LoadLocales).Wait();
        }

        var country = langCode switch
        {
            "es" => "ES",
            "fr" => "FR",
            _ => string.Empty,
        };

        var r =
            (from l in _locales
            where l.Language.Equals(langCode)
                && l.Country.Equals(country)
            select l).ToList();

        if (!r.Any())
        {
            throw new ApplicationException(
                "Requested TTS locale not found for " + langCode);
        }
        
        return r.First();
    }
}
