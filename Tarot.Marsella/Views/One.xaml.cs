﻿using Tarot.Marsella.Models;
using Tarot.Marsella.ViewModels;
using Tarot.Marsella.Helpers;

namespace Tarot.Marsella;

public partial class One : ContentPage
{
    private new OneViewModel BindingContext 
        => base.BindingContext as OneViewModel;

    public One()
    {
        base.BindingContext = ServiceHelper.GetService<OneViewModel>();
        InitializeComponent();
        AttachBindingContextEvents();
        BindingContextChanged += (_, _) => AttachBindingContextEvents();
        ManualMode.CommandParameter = this;
    }

    private void AttachBindingContextEvents()
    {
        BindingContext.PropertyChanged += (s, e) =>
        {
            if (!e.PropertyName.StartsWith("Mood"))
            {
                return;
            }

            if (!int.TryParse(e.PropertyName.Replace("Mood", string.Empty),
                out var index))
            {
                throw new ArgumentException(
                    $"{e.PropertyName} is not a valid property.");
            }

            ReadMoodIndex(index);
        };
    }

    private void ReadMoodIndex(int index) 
    {
        Border like, neutral, dislike;
        like = index switch
        {
            1 => LikeBorder1,
            2 => LikeBorder1,
            3 => LikeBorder1,
            _ => throw new ArgumentException($"Card index {index} is not valid")
        };
        neutral = index switch
        {
            1 => NeutralBorder1,
            2 => NeutralBorder1,
            3 => NeutralBorder1,
            _ => throw new ArgumentException($"Card index {index} is not valid")
        };
        dislike = index switch
        {
            1 => DislikeBorder1,
            2 => DislikeBorder1,
            3 => DislikeBorder1,
            _ => throw new ArgumentException($"Card index {index} is not valid")
        };

        var mood = index switch
        {
            1 => BindingContext.Mood1,
            2 => BindingContext.Mood1,
            3 => BindingContext.Mood1,
            _ => throw new ArgumentException($"Card index {index} is not valid")
        };

        switch (mood)
        {
            case Moods.Like:
                like.Stroke = Brush.Black;
                neutral.Stroke = Brush.Transparent;
                dislike.Stroke = Brush.Transparent;
                break;
            case Moods.Neutral:
                like.Stroke = Brush.Transparent;
                neutral.Stroke = Brush.Black;
                dislike.Stroke = Brush.Transparent;
                break;
            case Moods.Dislike:
                like.Stroke = Brush.Transparent;
                neutral.Stroke = Brush.Transparent;
                dislike.Stroke = Brush.Black;
                break;
        }
    }
}
