﻿using Microsoft.Extensions.Logging;
using Microsoft.Maui.LifecycleEvents;
using Tarot.Marsella.Helpers;
using Tarot.Marsella.Services;
using Tarot.Marsella.ViewModels;

namespace Tarot.Marsella;

public static class MauiProgram
{
    public static MauiApp CreateMauiApp()
    {
        var builder = MauiApp.CreateBuilder();
        builder
            .UseMauiApp<App>()
            .ConfigureLifecycleEvents(events =>
            {
#if WINDOWS && DEBUG
                events.AddWindows(windows => {
                    windows.OnWindowCreated(window =>
                    {
                        window.SizeChanged += (s, e) =>
                        {
                            System.Diagnostics.Debug.WriteLine(
                                $"W: {e.Size.Width}, H: {e.Size.Height}");
                        };
                    });
                });
#endif
            })
            .ConfigureFonts(fonts =>
            {
                fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
            });

        builder.Services.AddTransient<IGenAI, GPTOpenAI>();
        builder.Services.AddSingleton<TarotReader>();
        builder.Services.AddTransient<OneViewModel>();
        builder.Services.AddTransient<TriageViewModel>();

        var app = builder.Build();
        ServiceHelper.Initialize(app.Services); // register IServiceProvicer
        return app;
    }
}