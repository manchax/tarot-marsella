﻿using System.Diagnostics;
using Tarot.Marsella.Models;
using Tarot.Marsella.Services;

namespace Tarot.Marsella.ViewModels;

internal class TriageViewModel(TarotReader reader)
    : TarotViewModel(reader)
{
    private ArcanoMayor _carta1, _carta2, _carta3;

    private readonly Moods[] _moods =
    [
        Moods.Neutral,
        Moods.Neutral,
        Moods.Neutral
    ];

    private Keyword[] _keywords = [];

    private string
        _keyword1 = string.Empty,
        _keyword2 = string.Empty,
        _keyword3 = string.Empty;

    private ImageSource _imgC1, _imgC2, _imgC3;

    private string _caption = string.Empty;

    public string Caption
    {
        get => _caption;
        private set => SetProperty(ref _caption, value);
    }

    public ImageSource Carta1
    {
        get => _imgC1;
        private set => SetProperty(ref _imgC1, value);
    }

    public ImageSource Carta2
    {
        get => _imgC2;
        private set => SetProperty(ref _imgC2, value);
    }

    public ImageSource Carta3
    {
        get => _imgC3;
        private set => SetProperty(ref _imgC3, value);
    }

    public string Keyword1
    {
        get => _keyword1;
        private set => SetProperty(ref _keyword1, value);
    }

    public string Keyword2
    {
        get => _keyword2;
        private set => SetProperty(ref _keyword2, value);
    }

    public string Keyword3
    {
        get => _keyword3;
        private set => SetProperty(ref _keyword3, value);
    }

    public Moods Mood1
    {
        get => _moods[0];
        private set => SetProperty(ref _moods[0], value);
    }

    public Moods Mood2
    {
        get => _moods[1];
        private set => SetProperty(ref _moods[1], value);
    }

    public Moods Mood3
    {
        get => _moods[2];
        private set => SetProperty(ref _moods[2], value);
    }

    protected override void CreateCommands()
    {
        Tirar = new Command(async () =>
        {
            _reader.Barajar();
            Keyword1 = Keyword2 = Keyword3 = string.Empty;

            var (c1, c2, c3) = await _reader.Tirar3();
            _carta1 = c1;
            _carta2 = c2;
            _carta3 = c3;

            Carta1 = ImageSource.FromFile(_carta1.ImageFileName);
            Carta2 = ImageSource.FromFile(_carta2.ImageFileName);
            Carta3 = ImageSource.FromFile(_carta3.ImageFileName);
        });

        PickRandomKeywords = new Command(() =>
        {
            _keywords = _reader.PickRandomKeywords(
                (_carta1, Mood1),
                (_carta2, Mood2),
                (_carta3, Mood3)
            );
            Keyword1 = _keywords[0].Word;
            Keyword2 = _keywords[1].Word;
            Keyword3 = _keywords[2].Word;
        });

        RepeatKeywords = new Command(() =>
        {
            if (_keywords.Length == 0)
            {
                return;
            }
            _reader.SayKeywords([(_carta1, Mood1),
                (_carta2, Mood2), (_carta3, Mood3)],
                _keywords);
        });

        SayCards = new Command(async () =>
        {
            _reader.Silence();
            await _reader.SayCards(_carta1, _carta2, _carta3);
        });

        DescribeCard = new Command<string>(async number =>
        {
            Trace.TraceInformation($"{nameof(DescribeCard)} command started.");
            // pick card based on command argument (string)
            var card = number switch
            {
                "0" => _carta1,
                "1" => _carta2,
                "2" => _carta3,
                _ => throw new NotImplementedException()
            };

            _reader.Silence();
            await _reader.SayCard(card).ContinueWith(async t =>
            {
                if (t.IsCanceled)
                {
                    return;
                }

                await _reader.SayKeywords(card);
            });
            Trace.TraceInformation($"{nameof(DescribeCard)} command finished.");
        });

        SelectMood = new Command<string>((args) =>
        {
            Debug.WriteLine(args);
            var splitted = args.Split('_');
            if (!Enum.TryParse(typeof(Moods), splitted[0],
                out var moodObj))
            {
                throw new ArgumentException($"Mood '{splitted[0]}' is invalid.",
                    nameof(args));
            }

            var mood = (Moods)(moodObj ??= Moods.Neutral);
            if (!int.TryParse(splitted[1], out var index))
            {
                throw new ArgumentException($"Card # '{splitted[1]}' is invalid.",
                    nameof(args));
            }

            switch (index)
            {
                case 1:
                    Mood1 = mood;
                    break;
                case 2:
                    Mood2 = mood;
                    break;
                case 3:
                    Mood3 = mood;
                    break;
            }
        });

        ManualMode = new Command<Triage>(async page =>
        {
            var carta1 = await page.DisplayPromptAsync("Carta 1",
                "¿Qué carta desea sacar como Carta #1? (0-21)");
            var carta2 = await page.DisplayPromptAsync("Carta 2",
                "¿Qué carta desea sacar como Carta #2? (0-21)");
            var carta3 = await page.DisplayPromptAsync("Carta 3",
                "¿Qué carta desea sacar como Carta #3? (0-21)");

            if (!int.TryParse(carta1, out var icarta1))
            {
                await page.DisplayAlert(
                    "Error en Carta 1",
                    "Valor de la Carta 1 es inválido. Intente de nuevo.", "OK");
                return;
            }

            if (!int.TryParse(carta2, out var icarta2))
            {
                await page.DisplayAlert(
                    "Error en Carta 2",
                    "Valor de la Carta 2 es inválido. Intente de nuevo.", "OK");
                return;
            }

            if (!int.TryParse(carta3, out var icarta3))
            {
                await page.DisplayAlert(
                    "Error en Carta 3",
                    "Valor de la Carta 3 es inválido. Intente de nuevo.", "OK");
                return;
            }

            try
            {
                _carta1 = _reader.BuscarCarta(icarta1);
                _carta2 = _reader.BuscarCarta(icarta2);
                _carta3 = _reader.BuscarCarta(icarta3);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                await page.DisplayAlert(
                    "Error en alguna de las Cartas",
                    $"Valor de entrada es inválido. Intente de nuevo. Detalles: {ex.Message}.", "OK");
                return;
            }

            Carta1 = ImageSource.FromFile(_carta1.ImageFileName);
            Carta2 = ImageSource.FromFile(_carta2.ImageFileName);
            Carta3 = ImageSource.FromFile(_carta3.ImageFileName);

            _reader.Tirar3(_carta1, _carta2, _carta3);
        });

        base.CreateCommands();
    }
}
