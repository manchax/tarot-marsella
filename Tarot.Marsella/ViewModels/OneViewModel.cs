﻿using System.Diagnostics;
using Tarot.Marsella.Models;
using Tarot.Marsella.Services;

namespace Tarot.Marsella.ViewModels;

internal class OneViewModel(TarotReader reader)
    : TarotViewModel(reader)
{
    private ArcanoMayor _carta1;
    private ImageSource _imgC1;
    private Moods _mood1 = Moods.Neutral;
    private Keyword _keyword1;

    public ImageSource Carta1
    {
        get => _imgC1;
        private set => SetProperty(ref _imgC1, value);
    }

    public Moods Mood1
    {
        get => _mood1;
        private set => SetProperty(ref _mood1, value);
    }

    public string Keyword1 => _keyword1?.Word ?? string.Empty;

    /// <summary>
    /// Custom Commands for One Card Tab.
    /// </summary>
    /// <remarks>
    /// These are mapped to Buttons in the UI.
    /// </remarks>
    /// <seealso cref="TarotViewModel.CreateCommands"/> 
    protected override void CreateCommands()
    {
        Tirar = new Command(async () =>
        {
            _reader.Barajar();
            _carta1 = await _reader.Tirar1();
            Carta1 = ImageSource.FromFile(_carta1.ImageFileName);
        });

        PickRandomKeywords = new Command(() =>
        {
            _keyword1 = _reader.PickRandomKeywords(
                (_carta1, Mood1)
            )[0];
        });

        RepeatKeywords = new Command(() =>
        {
            _reader.SayKeywords([(_carta1, Mood1)], [_keyword1]);
        });

        SayCards = new Command(async () =>
        {
            _reader.Silence();
            await _reader.SayCards(_carta1);
        });

        DescribeCard = new Command<string>(async number =>
        {
            Trace.TraceInformation($"{nameof(DescribeCard)} command started.");
            _reader.Silence();
            await _reader.SayCard(_carta1).ContinueWith(async t =>
            {
                if (t.IsCanceled)
                {
                    return;
                }

                await _reader.SayKeywords(_carta1);
            });
            Trace.TraceInformation($"{nameof(DescribeCard)} command finished.");
        });

        SelectMood = new Command<string>((args) =>
        {
            Debug.WriteLine(args);
            var splitted = args.Split('_');
            if (!Enum.TryParse(typeof(Moods), splitted[0],
                out var moodObj))
            {
                throw new ArgumentException($"Mood '{splitted[0]}' is invalid.",
                    nameof(args));
            }

            var mood = (Moods)(moodObj ??= Moods.Neutral);
            if (!int.TryParse(splitted[1], out var index))
            {
                throw new ArgumentException($"Card # '{splitted[1]}' is invalid.",
                    nameof(args));
            }

            switch (index)
            {
                case 1:
                    Mood1 = mood;
                    break;
            }
        });

        ManualMode = new Command<One>(async page =>
        {
            var carta1 = await page.DisplayPromptAsync("Carta Manual",
                "¿Qué carta desea sacar? (0-21)");

            if (!int.TryParse(carta1, out var iCarta1))
            {
                await page.DisplayAlert(
                    "Error en Carta",
                    "Valor de la Carta es inválido. Intente de nuevo.", "OK");
                return;
            }

            try
            {
                _carta1 = _reader.BuscarCarta(iCarta1);
                Carta1 = ImageSource.FromFile(_carta1.ImageFileName);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                await page.DisplayAlert("Error",
                    $"Valor de entrada es inválido. Intente de nuevo. Detalles: {ex.Message}.",
                    "OK");
                return;
            }
            catch (Exception ex)
            {
                await page.DisplayAlert("Error",
                    $"Error Genérico. Detalles: {ex.Message}.", "OK");
                return;
            }

            _reader.Tirar1(_carta1);
        });

        base.CreateCommands();
    }
}