﻿using System.Windows.Input;
using Tarot.Marsella.Services;

namespace Tarot.Marsella.ViewModels;

internal abstract class TarotViewModel : ViewModelBase
{
    protected readonly TarotReader _reader;

    protected TarotViewModel(TarotReader reader)
    {
        _reader = reader;
        CreateCommands();
        Tirar.Execute(null);
    }

    public ICommand Tirar
    {
        get; protected set;
    }

    public ICommand DescribeCard
    {
        get; protected set;
    }

    public ICommand SayCards
    {
        get; protected set;
    }

    public ICommand PickRandomKeywords
    {
        get; protected set;
    }

    public ICommand RepeatKeywords
    {
        get; protected set;
    }

    public ICommand SelectMood
    {
        get; protected set;
    }

    public ICommand ManualMode
    {
        get; protected set;
    }

    public ICommand Silenciar
    {
        get; private set;
    }

    protected virtual void CreateCommands()
    {
        Silenciar = new Command(_reader.Silence);
    }
}
