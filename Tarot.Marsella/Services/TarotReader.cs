﻿using System;
using System.Diagnostics;
using Tarot.Marsella.Models;

namespace Tarot.Marsella.Services;

using ArcanoMood = (ArcanoMayor Carta, Moods Mood);
using TripleArcano = (ArcanoMayor uno, ArcanoMayor dos, ArcanoMayor tres);

/// <summary>
/// Provides logic to randomize the deck.
/// Main method is <see cref="Tirar3()"/>.
/// </summary>
internal partial class TarotReader
{
    /// <summary>
    /// Number of times that the deck will be shuffled.
    /// </summary>
    private const int TimesShuffled = 2;

    /// <summary>
    /// Total number of major arcane cards.
    /// </summary>
    private const int NumberOfCards = 22;

    /// <summary>
    /// Voice language code for Spanish.
    /// </summary>
    private const string LangCodeES = "es";

    /// <summary>
    /// Voice language code for French.
    /// </summary>
    private const string LangCodeFR = "fr";

    /// <summary>
    /// French conjunction 'et'.
    /// </summary>
    private const string ConjunctionFR = "et ";

    /// <summary>
    /// Spanish conjunction 'y'.
    /// </summary>
    private const string ConjunctionES = "y ";

    /// <summary>
    /// Enables/disables choicing random voices
    /// installed on the system.
    /// </summary>
    private const bool EnableRandomVoices = true;

    private const int Min = 0;
    private const int Max = 22;

    private readonly IGenAI _genAI;
    private readonly Random _random = new();

    /// <summary>
    /// Major arcane deck.
    /// </summary>
    private IList<ArcanoMayor> _baraja
        = new ArcanoMayor[NumberOfCards];

    /// <summary>
    /// Random keywords for each card picked.
    /// </summary>
    private Keyword[] _keywords = new Keyword[3];

    private IEnumerable<Locale> _locales = [];
    private CancellationTokenSource _ctsStopSpeech = new();

    /// <summary>
    /// Default .ctor()
    /// </summary>
    public TarotReader(IGenAI genAI)
    {
        _genAI = genAI;
        _ = Task.Run(LoadLocales);
        BuildDeck();
    }

    private async Task LoadLocales()
    {
        _locales = await TextToSpeech.Default.GetLocalesAsync();
#if DEBUG
        _locales.ToList().ForEach(l =>
            Debug.WriteLine("Name = {0}, Language = {1}, Country = {2}",
                l.Name, l.Language, l.Country));
#endif
    }

    private void BuildDeck()
    {
        var files = new string[] {
            "arc0_lemat.png",
            "arc1_lebateleur.png",
            "arc10_la_roue_de_fortune.png",
            "arc11_laforce.png",
            "arc12_lependu.png",
            "arc13.png",
            "arc14_temperance.png",
            "arc15_lediable.png",
            "arc16_lamaison_dieu.png",
            "arc17_letoille.png",
            "arc18_lalune.png",
            "arc19_lesoleil.png",
            "arc2_lapapesse.png",
            "arc20_lejugement.png",
            "arc21_lemonde.png",
            "arc3_limperatrice.png",
            "arc4_lempereur.png",
            "arc5_lepape.png",
            "arc6_lamoureux.png",
            "arc7_lechariot.png",
            "arc8_lajustice.png",
            "arc9_lhermite.png",
        };
        var index = 0;
        foreach (var file in files)
        {
            var filename = Path.GetFileName(file);
            var components = filename.Contains('_')
                ? filename.Split('_')
                : ["13", "ARCANO 13"];

            var numero = components[0].Replace("arc", string.Empty);
            var nombre = components[1];

            _baraja[index++] = new ArcanoMayor(nombre,
                numero: byte.Parse(numero),
                filename);
        }
    }

    public void Barajar()
    {
        for (var i = 0; i < TimesShuffled; i++)
        {
            Surtir();
        }

        void Surtir()
        {
            var source = _baraja.AsEnumerable().ToList(); // make a copy
            List<ArcanoMayor> destiny = [];
            int index;
            do // shuffle deck, save it in destiny
            {
                index = _random.Next(0, source.Count);
                ArcanoMayor card;
                try { card = source[index]; }
                catch
                {
#if DEBUG
                    Debug.WriteLine(index);
                    throw;
#else
                    card = source[0];
#endif
                }
                if (destiny.Exists(b => b.Numero == card.Numero))
                {
                    continue;
                }

                MoveCard(card, source, destiny);
            } while (source.Count > 1);

            MoveCard(source[0], source, destiny); // move last one
            _baraja = destiny; // ready
        }
    }

    private static void MoveCard(ArcanoMayor card, List<ArcanoMayor> source,
        List<ArcanoMayor> destiny)
    {
        destiny.Add(card);
        source.Remove(card);
    }

    public Task<ArcanoMayor> Tirar1()
    {
        var cual = _random.Next(Min, Max);
        var card = _baraja.First(c => c.Numero == cual);
        Tirar1(card);
        return Task.FromResult(card);
    }

    public void Tirar1(ArcanoMayor carta)
    {
        _ = Task.Run(async () => await SayCards(carta),
            _ctsStopSpeech.Token);
    }

    public async Task<TripleArcano> Tirar3()
    {
        var result = await Task.Run(() =>
        {
            ArcanoMayor uno, dos, tres;
            ArcanoMayor dameLa(int num) =>
                _baraja.First(c => c.Numero == num);

            var cual = _random.Next(Min, Max);
            uno = dameLa(cual);

            do
            {
                cual = _random.Next(Min, Max);
            } while (cual == uno.Numero); // uniqueness

            dos = dameLa(cual);

            do
            {
                cual = _random.Next(Min, Max);
            } while (cual == dos.Numero
            || cual == uno.Numero);

            tres = dameLa(cual);
            Debug.WriteLine($"{uno.Numero}, {dos.Numero} y {tres.Numero}");
            return (uno, dos, tres); // result
        });

        _ = Task.Run(async () => await SayCards(
                result.uno,
                result.dos,
                result.tres),
            _ctsStopSpeech.Token); // say them

        return result;
    }

    public void Tirar3(ArcanoMayor uno, ArcanoMayor dos, ArcanoMayor tres)
    {
        _ = Task.Run(async () =>
            await SayCards(uno, dos, tres)
        );
    }

    public ArcanoMayor BuscarCarta(int carta) => carta switch
    {
        < 0 or > 21 => throw new ArgumentOutOfRangeException(nameof(carta),
            $"Número de carta inválido ({carta})."),
        >= 0 or <= 21 => _baraja.First(a => a.Numero == carta),
    };

    public async Task SayCard(ArcanoMayor arcano)
        => await SayCardInternal(arcano);

    public Keyword[] PickRandomKeywords(params ArcanoMood[] arcanos)
    {
        _keywords = new Keyword[arcanos.Length];

        for (var i = 0; i < arcanos.Length; i++)
        {
            _keywords[i] = GrabRandomKeyword(arcanos[i].Carta);
        }

        SayKeywords(arcanos, _keywords);

        return _keywords;
    }

    public void Silence()
    {
        if (_ctsStopSpeech?.IsCancellationRequested ?? true)
        {
            _ctsStopSpeech = new CancellationTokenSource();
            return;
        }

        var stackTrace = new StackTrace();
        var method = stackTrace.GetFrame(1).GetMethod();
        var methodName = method.Name;
        var className = method.ReflectedType.Name;

        Trace.TraceInformation(
            "{0}: Cancelling all tasks. Caller method: {1}",
            nameof(Silence), className + "." + methodName);

        _ctsStopSpeech.Cancel();
        _ctsStopSpeech = new CancellationTokenSource();
    }

    public Task SayKeywords(ArcanoMayor carta) => Task.Run(async () =>
    {
        var t = Speak($"A {carta.GetName(LangCodeES)} se le asocia con: ");
        foreach (var keyword in carta.Keywords)
        {
            t = t.ContinueWith(async t =>
            {
                if (t.IsCanceled)
                {
                    return;
                }
                await Speak(keyword.Word);
            }, _ctsStopSpeech.Token);
        }
        await t;
    }, _ctsStopSpeech.Token);

    internal async Task SayCards(params ArcanoMayor[] arcanos)
    {
        var i = 0;
        foreach (var a in arcanos)
        {
            var isLastOfMany = arcanos.Length > 1 && i == arcanos.Length - 1;
            await SayCardInternal(a, isLastOfMany);
            i++;
        }
    }

    private partial Locale FindLocale(string langCode);

    private async Task Speak(string text, Locale locale) =>
        await Speak(text, new SpeechOptions { Locale = locale });

    private async Task Speak(string text, SpeechOptions options = null)
    {
        if (_ctsStopSpeech.IsCancellationRequested)
        {
            return;
        }

        try
        {
            if (options is not null)
            {
                await TextToSpeech.SpeakAsync(text, options,
                    _ctsStopSpeech.Token);
            }
            else
            {
                await Speak(text, FindLocale(LangCodeES));
            }
        }
        catch (TaskCanceledException)
        {
            return;
        }
        catch (OperationCanceledException)
        {
            return;
        }
    }

    private static string DescribeMood(Moods mood, Keyword word) => mood switch
    {
        Moods.Like => string.Format("{0} {1}", word.Positive, word.Word),
        Moods.Dislike => string.Format("{0} {1}", word.Negative, word.Word),
        Moods.Neutral or _ => word.Word,
    };

    public void SayKeywords(ArcanoMood[] arcanos, Keyword[] keywords) => Task.Run(async () =>
    {
        if (arcanos.Length == 3)
        {
            for (var i = 0; i < 3; i++)
            {
                var moodDesc = DescribeMood(arcanos[i].Mood, keywords[i]);
                _ = i switch
                {
                    0 => Speak("Origen, pasado o historia: " + moodDesc),
                    1 => Speak("Situación actual: " + moodDesc),
                    2 => Speak("Resolución o salida: " + moodDesc),
                    _ => throw new NotImplementedException()
                };
            }
            //// test with Chat GPT3
            //var question = string.Format(
            //    "Dame un consejo para un problema relacionado con {0} en el pasado que haya originado {1} en la actualidad y que se resuelva con {2}",
            //    DescribeMood(arcanos[0].Mood, _keywords[0]),
            //    DescribeMood(arcanos[1].Mood, _keywords[1]),
            //    DescribeMood(arcanos[2].Mood, _keywords[2]));
            //Debug.WriteLine(question);
            foreach (var text in _genAI.CreateStory(
                DescribeMood(arcanos[0].Mood, _keywords[0]),
                DescribeMood(arcanos[1].Mood, _keywords[1]),
                DescribeMood(arcanos[2].Mood, _keywords[2])
            ))
            {
                Debug.WriteLine(text);
                await Speak(text);
            }

        }
        else if (arcanos.Length == 1)
        {
            await Speak(DescribeMood(arcanos[0].Mood, keywords[0]));
        }
    }, _ctsStopSpeech.Token);

    private async Task AppendNumeroAndName(ArcanoMayor a, string langCode,
        Locale locale)
    {
        var (fr, sp) = ArcanoMayor.GetName(a.Numero);
        var name = langCode switch
        {
            "es" => sp,
            "fr" => fr,
            _ => a.Nombre
        };
        await Speak(a.Numero.ToString(), locale).ContinueWith(async t =>
        {
            if (t.IsCanceled)
            {
                return;
            }
            await Speak(name, locale);
        }, _ctsStopSpeech.Token);
    }

    private async Task SayCardInternal(ArcanoMayor a, bool isLast = false)
    {
        var es = FindLocale(LangCodeES);
        var fr = FindLocale(LangCodeFR);

        try
        {
            if (isLast)
            {
                await Speak(ConjunctionFR, fr);
            }

            await AppendNumeroAndName(a, LangCodeFR, fr);

            if (isLast)
            {
                await Speak(ConjunctionES, es);
            }

            await AppendNumeroAndName(a, LangCodeES, es);
        }
        catch (Exception ex)
        {
            Debug.WriteLine("Exception catched at {0}. Exception Type: {1}",
                nameof(SayCardInternal), ex.GetType());
            Debug.WriteLine(ex);
            return;
        }

    }

    private Keyword GrabRandomKeyword(ArcanoMayor arcano)
    {
        var index = _random.Next(arcano.Keywords.Length);
        return arcano.Keywords[index];
    }
}