﻿using System.ClientModel;
using OpenAI;
using OpenAI.Chat;

namespace Tarot.Marsella.Services;
internal class GPTOpenAI : IGenAI
{
    private static readonly ChatCompletionOptions ChatOptions = new()
    {
        Temperature = 1.0f,
        TopP = 1.0f,
        /* MaxOutputTokenCount = 1000 */
        /* MaxTokens = 1000 */
    };

    private readonly ChatClient _client;

    public GPTOpenAI()
    {
        var apiKey = Environment.GetEnvironmentVariable("GITHUB_TOKEN")
            ?? throw new NullReferenceException("GITHUB_TOKEN environment variable is not set.");
        var options = new OpenAIClientOptions()
        {
            Endpoint = new Uri("https://models.inference.ai.azure.com")
        };
        _client = new ChatClient("gpt-4o", new ApiKeyCredential(apiKey), options);
    }

    public IEnumerable<string> CreateStory(string word1, string word2, string word3)
    {
        var prompt = $"Haz una historia con estas 3 palabras: {word1}, {word2} y {word3}";
        var response = _client.CompleteChat([new UserChatMessage(prompt)], ChatOptions);
        foreach (var part in response.Value.Content)
        {
            yield return part.Text;
        }
    }

    public async IAsyncEnumerable<string> CreateStoryAsync(string word1, string word2, string word3)
    {
        var prompt = $"Haz una historia con estas 3 palabras: {word1}, {word2} y {word3}";
        var response = await _client.CompleteChatAsync([new UserChatMessage(prompt)], ChatOptions);
        foreach (var part in response.Value.Content)
        {
            yield return part.Text;
        }
    }
}