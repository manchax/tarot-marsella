﻿
namespace Tarot.Marsella.Services;

internal interface IGenAI
{
    IEnumerable<string> CreateStory(string word1, string word2, string word3);

    IAsyncEnumerable<string> CreateStoryAsync(string word1, string word2, string word3);
}