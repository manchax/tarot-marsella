﻿namespace Tarot.Marsella.Models;

public struct ArcanoMayor
{
    public ArcanoMayor(string nombre, byte numero, string fileName)
    {
        Nombre = GetName(numero).fr;
        if (string.IsNullOrWhiteSpace(Nombre))
        {
            Nombre = nombre;
        }

        Numero = numero;
        ImageFileName = fileName;
        Keywords = LoadKeywords(numero);
    }

    public string Nombre
    {
        get;
    }

    public byte Numero
    {
        get;
    }

    public string ImageFileName
    {
        get;
    }

    public Keyword[] Keywords
    {
        get;
    }

    public static (string fr, string sp) GetName(byte numero) => numero switch
    {
        0 => ("Le mat", "El loco"),
        1 => ("Le bateleur", "El mago"),
        2 => ("La papesse", "La papisa"),
        3 => ("L'imperatrice", "La emperatriz"),
        4 => ("L'empereur", "El emperador"),
        5 => ("Le pape", "El papa"),
        6 => ("L'amoureux", "Los enamorados"),
        7 => ("Le chariot", "El carro"),
        8 => ("La justice", "La justicia"),
        9 => ("L'hermite", "El ermitaño"),
        10 => ("La roue de fortune", "La rueda de la fortuna"),
        11 => ("La force", "La fuerza"),
        12 => ("Le pendu", "El colgado"),
        13 => ("L'Arcane sans nom", "Arcano sin Nombre o Arcano 13"),
        14 => ("Tempérance", "Templanza"),
        15 => ("Le diable", "El diablo"),
        16 => ("La maison dieu", "La casa Dios"),
        17 => ("L'étoile", "La estrella"),
        18 => ("La lune", "La luna"),
        19 => ("Le soleil", "El sol"),
        20 => ("Le jugement", "El juicio"),
        21 => ("Le monde", "El mundo"),
        _ => throw new ArgumentOutOfRangeException(nameof(numero)),
    };

    public readonly string GetName(string langCode = "fr") => langCode switch
    {
        "fr" => GetName(Numero).fr,
        _ => GetName(Numero).sp,
    };

    private static Keyword[] LoadKeywords(byte numero)
    {
        List<Keyword> keywords = [];

        var filename = string.Format(Path.Combine(
            "MayorKeys", "claves_{0}.txt"), numero);

        Task.Run(async () =>
        {
            try
            {
                using var fstream = await FileSystem.Current.OpenAppPackageFileAsync(filename);
                using var reader = new StreamReader(fstream);
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var fields = line.Trim().Split(',');
                    if (fields.Length == 3)
                    {
                        keywords.Add(
                            new Keyword(fields[0], fields[1], fields[2]));
                    }
                    else
                    {
                        keywords.Add(new Keyword(fields[0], string.Empty, string.Empty));
                    }
                }
            }
            catch (FileNotFoundException)
            {
                keywords = [];
            }
        }).Wait();

        return [.. keywords];
    }
}
