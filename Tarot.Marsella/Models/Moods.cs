﻿namespace Tarot.Marsella.Models;

public enum Moods : byte
{
    Like,
    Neutral,
    Dislike
}