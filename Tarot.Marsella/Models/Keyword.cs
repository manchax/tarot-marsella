﻿namespace Tarot.Marsella.Models;

public record Keyword(
    string Word,
    string Positive,
    string Negative
);