﻿namespace Tarot.Marsella.Helpers;

/// <summary>
/// Encapsulates a Singleton instance of <see cref="IServiceProvider"/>.
/// </summary>
public static class ServiceHelper
{
    private static IServiceProvider Services
    {
        get; set;
    } = null;

    public static void Initialize(IServiceProvider serviceProvider) =>
        Services = serviceProvider;

    public static T GetService<T>() => Services.GetService<T>();
}